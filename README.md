## NAME
	trace - print the route to network host
## SYNOPSIS
	myripsniffer -i <rozhraní> 
	 -i: <rozhraní> udává rozhraní, na kterém má být odchyt paketů prováděn.

## SYNOPSIS
	myripresponse -i <rozhraní> -r <IPv6>/[16-128] {-n <IPv6>} {-m [0-16]} {-t [0-65535]} 
		-i: <rozhraní> udává rozhraní, ze kterého má být útočný paket odeslán;
		-r: v <IPv6> je IP adresa podvrhávané sítě a za lomítkem číselná délka masky sítě;
		-m: následující číslo udává RIP Metriku, tedy počet hopů, implicitně 1;
		-n: <IPv6> za tímto parametrem je adresa next-hopu pro podvrhávanou routu, implicitně ::;
		-t: číslo udává hodnotu Router Tagu, implicitně 0.

## DESCRIPTION
trace is a utility that records the route (the specific gateway computers at each hop) through the Internet between your computer and a specified destination computer. It also calculates and displays the amount of time each hop took. Trace can work with IPv4 and IPv6 address, and with host DNS name (only IPv4 route).

## OPTIONS
	-f	Specifies with what TTL to start. Defaults to 1.
	-m	Specifies with what TTL to end. Defaults to 30.


## IMPLEMENTATION
trace is inplemented in C++ using  netinet library. Whole program runs in cycle where each cycle pass is TTL incremented by 1 and UDP packet send to desired  host. ICMP respond is processed to figure out IP and DNS name (if possible). The timeout to recieve respond message is 2 seconds, then the '*' is printed on output ( stdio ). The packet is send using  sendto()  function, respond is recieved with  recvmsg()  and then processed using CMSG macros. Whole process is quite similar while using IPv6 adress except of switchng to IPv6 data structures. Processing ICMP message consists of testing on: ICMP_NET_UNREACH - network unreachale (output N!), ICMP_HOST_UNREACH - host unreachale (output H!), ICMP_PROT_UNREACH - protocol unreachale (output P!) and ICMP_PKT_FILTERED - communication administratively prohibited (output X!), if none of these errors occured, the IP route informations are printed.

## EXAMPLE
	user@merlin: ~/traceroute$ ./trace 216.58.201.100
	1	147.229.176.1 (147.229.176.1)	7.000 ms
	2	bda-boz.fit.vutbr.cz (147.229.254.217)	6.000 ms
	3	pe-meo.net.vutbr.cz (147.229.252.90)	5.000 ms
	4	pe-ant.net.vutbr.cz (147.229.253.233)	1.000 ms
	5	rt-ant.net.vutbr.cz (147.229.252.17)	3.000 ms
	6	r98-bm.cesnet.cz (195.113.235.109)	47.000 ms
	7	195.113.157.70 (195.113.157.70)	24.000 ms
	8	r2-r93.cesnet.cz (108.170.245.49)	25.000 ms
	9	108.170.238.233 (108.170.238.233)	24.000 ms
	10	216.58.201.100 (216.58.201.100)	24.000 ms   


	user@merlin: ~/traceroute$ ./trace www.google.com
	1	147.229.176.1 (147.229.176.1)	9.000 ms
	2	bda-boz.fit.vutbr.cz (147.229.254.217)	6.000 ms
	3	pe-meo.net.vutbr.cz (147.229.252.90)	4.000 ms
	4	pe-ant.net.vutbr.cz (147.229.253.233)	1.000 ms
	5	rt-ant.net.vutbr.cz (147.229.252.17)	4.000 ms
	6	r98-bm.cesnet.cz (195.113.235.109)	43.000 ms
	7	195.113.157.70 (195.113.157.70)	25.000 ms
	8	r2-r93.cesnet.cz (108.170.245.33)	24.000 ms
	9	108.170.238.235 (108.170.238.235)	24.000 ms
	10	216.58.201.100 (216.58.201.100)	24.000 ms   


	user@merlin: ~/traceroute$ ./trace 2001:4860:4860::8888
	1	2001:67c:1220:8b0::1 (2001:67c:1220:8b0::1)	4.000 ms
	2	bda-boz6.fit.vutbr.cz (2001:67c:1220:f521::aff:4901)	4.000 ms
	3	pe-meo.ipv6.net.vutbr.cz (2001:67c:1220:f734::aff:4a)	5.000 ms
	4	pe-tech.ipv6.net.vutbr.cz (2001:67c:1220:f724::aff:4801)	4.000 ms
	5	pe-kou.ipv6.net.vutbr.cz (2001:67c:1220:f534::1)	1.000 ms
	6	rt-kou.ipv6.net.vutbr.cz (2001:718:0:c003::1)	29.000 ms
	7	2001:718:0:600:0:118:119:11 (2001:718:0:600:0:118:119:11)	65.000 ms
	8	2001:7f8:14::1d:1 (2001:7f8:14::1d:1)	29.000 ms
	9	nixcz-v6.net.google.com (2001:4860:0:101a::1)	30.000 ms
	10	2001:4860:0:1::681 (2001:4860:0:1::681)	31.000 ms
	11	2001:4860:4860::8888 (2001:4860:4860::8888)	30.000 ms

## SEE ALSO
ping(8), ping6(8), netstat(8), traceroute(8)