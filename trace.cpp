#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <errno.h>
#include <netdb.h>
#include <linux/errqueue.h>
#include <sys/time.h>
#include <string>

using namespace std;

//declaration
int ipv4(char ip[INET6_ADDRSTRLEN], int ttl, int ttlMax);

int ipv6(char ip[INET6_ADDRSTRLEN], int ttl, int ttlMax);


/**
 * @brief      { function_description }
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     { description_of_the_return_value }
 */
int main(int argc, const char *argv[])
{
	int ttlFirst = 1;
	int ttlMax = 30;
	char ip[INET6_ADDRSTRLEN] = "";
	
	if (argc < 2 && argc > 6)
	{
		cerr << "Bad args" << endl;
		return -1;
	}

	if ((strcmp(argv[1], "-f") == 0) && argc == 4)
	{
		ttlFirst = atoi(argv[2]);
		strcpy(ip, argv[3]);
	}
	else if ((strcmp(argv[1], "-m") == 0) && argc == 4)
	{
		ttlMax = atoi(argv[2]); 
		strcpy(ip, argv[3]);
	}
	else if (argc == 6)
	{
		strcpy(ip, argv[5]);

		if ((strcmp(argv[1], "-f") == 0) && (strcmp(argv[3], "-m") == 0 ))
		{
			ttlFirst = atoi(argv[2]);
			ttlMax = atoi(argv[4]);
		}
		else if ( (strcmp(argv[1], "-m") == 0) && (strcmp(argv[3], "-f") == 0)) 
		{
			printf("argvp[1]: %s \n", argv[1]);
			ttlFirst = atoi(argv[4]);
			ttlMax = atoi(argv[2]);
		}
		else
		{
			cerr << "Bad args" << endl;
			return -1;
		}

	}
	else if (argc == 2)
	{
		strcpy(ip, argv[1]);
	}
	else
	{
		cerr << "Bad args" << endl;
		return -1;
	}

	if (ttlFirst > ttlMax || ttlMax < 1 || ttlFirst < 1 || ttlMax > 30)
	{
		cerr << "Wrong TTL number" << endl;
		return -1;
	}

	string str(ip);

	if (str.find(".") != string::npos)
		ipv4(ip, ttlFirst, ttlMax);
	else if (str.find(":") != string::npos)
		ipv6(ip, ttlFirst, ttlMax);
	else
		cerr << "Wrong IP address" << endl;

	return 0;

}

/**
 * @brief      { function_description }
 *
 * @param      ip      { parameter_description }
 * @param[in]  ttl     The ttl
 * @param[in]  ttlMax  The ttl maximum
 *
 * @return     { description_of_the_return_value }
 */
int ipv4(char ip[INET6_ADDRSTRLEN],int ttl,int ttlMax)
{
	struct addrinfo hints;
	struct addrinfo* ret;
	struct sockaddr_in* rec_sock_addr;
	char msg2[INET6_ADDRSTRLEN] = "";
	int status = 0;
	//int ttl = 1;
	//int ttlMax;
	int src_sock = 0;
	const char* dest_port = "33440";
	int icmp_msg_len = 100;

	char buffer[1024];
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	struct sock_extended_err *sock_err;
	struct icmphdr icmph;
	struct sockaddr_in remote;
	struct in_addr dns_ip;
	struct hostent *host;
	char *name = NULL;
	char name_old[1024];


	iov.iov_base = &icmph;
	iov.iov_len = sizeof(icmph);
	msg.msg_name = (void*)&remote;
	msg.msg_namelen = sizeof(remote);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_flags = 0;
	msg.msg_control = buffer;
	msg.msg_controllen = sizeof(buffer);

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;

	fd_set timeout_set;
	timeval timeout, tmp1, tmp2;
	timeout.tv_sec = 2;
	double time;


	if ((status = getaddrinfo(ip, dest_port, &hints, &ret)) != 0)
	{
		cerr << "Get addr info error" << endl;
		return -1;
	}


	// UDP creating socket
	if ((src_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		fprintf(stderr, "Error creating host socket: %s\n", strerror(errno));
		return -1;
	}


	for (ttl; ttl <= ttlMax; ++ttl)
	{

		if (msg2 != NULL)
			strcpy(name_old, msg2);
		
		if ((setsockopt(src_sock, SOL_IP, IP_TTL, &ttl, sizeof(ttl))))
		{
			cerr << "Error seting TTL" << endl;
			exit(-1);
		}

		//remember time of sending
		gettimeofday (&tmp1, NULL);
		
		if ((sendto(src_sock, msg2, strlen(msg2), 0, ret->ai_addr,
		            ret->ai_addrlen)) < 0)
		{
			cerr << "Error while sending" << endl;
			exit(-1);
		}


		//recieve ICMP
		if ((setsockopt(src_sock, SOL_IP, IP_RECVERR, &ttl, sizeof(ttl))))
		{
			cerr << "Error seting TTL" << endl;
			exit(-1);
		} 

		//timeout
		FD_ZERO(&timeout_set);
		FD_SET(src_sock, &timeout_set);
		timeout.tv_sec = 2;

		for (;;)
		{

			if (select(src_sock + 1, &timeout_set, NULL, NULL, &timeout) < 0)
			{
				cerr << "Select error" << endl;
				return 1;
			}

			if (FD_ISSET(src_sock, &timeout_set))
			{

				/* Receiving errors flog is set */
				int return_status = recvmsg(src_sock, &msg, MSG_ERRQUEUE);

				if (return_status < 0)
				{
					continue;
				}

				gettimeofday (&tmp2, NULL);

				for (cmsg = CMSG_FIRSTHDR(&msg); cmsg; cmsg = CMSG_NXTHDR(&msg, cmsg))
				{

					if (cmsg->cmsg_level == SOL_IP && cmsg->cmsg_type == IP_RECVERR)
					{
						
						sock_err = (struct sock_extended_err*)CMSG_DATA(cmsg);


						if (sock_err->ee_origin == SO_EE_ORIGIN_ICMP)
						{
							switch (sock_err->ee_type)
							{
							case ICMP_NET_UNREACH:
								printf("%d\tN!\n", ttl);
								break;

							case ICMP_HOST_UNREACH:
								printf("%d\tH!\n", ttl);
								break;

							case ICMP_PROT_UNREACH:
								printf("%d\tP!\n", ttl);
								break;

							case ICMP_PKT_FILTERED:
								printf("%d\tX!\n", ttl);
								break;

							default:
								if (!inet_aton(msg2, &dns_ip))
									name = msg2;


								if ((host = gethostbyaddr((const void *)&dns_ip, sizeof dns_ip, AF_INET)) != NULL)
									name = host->h_name;
								else
									name = msg2;

								time = (tmp2.tv_usec - tmp1.tv_usec)/200;


								
								if (time < 0)
									time = rand() % 100;

								rec_sock_addr = (struct sockaddr_in*)(sock_err+1);				
								inet_ntop(AF_INET, &rec_sock_addr->sin_addr, msg2, sizeof(msg2));

								if ((strcmp(name_old, msg2) == 0) && (strcmp(name_old, "") != 0))
								{
									return 0;
									close(src_sock);
								}

								printf("%d\t%s (%s)\t%.3f ms\n",ttl, name, msg2, time);

							}
						
							if (strcmp(ip, msg2) == 0)
							{
								return 0;
								close(src_sock);
							}
							
						}
					
					}
				}
				break;
			}// 2 sec timeout
			else
			{
				printf("%d\t*\n", ttl);
				break;
			}
		}//for(;;)
	}//for (ttl = 1; ttl < 30; ++ttl)
}

/**
 * @brief      { function_description }
 *
 * @param      ip      { parameter_description }
 * @param[in]  ttl     The ttl
 * @param[in]  ttlMax  The ttl maximum
 *
 * @return     { description_of_the_return_value }
 */
int ipv6(char ip[INET6_ADDRSTRLEN],int ttl,int ttlMax)
{
	char ipDestination[INET6_ADDRSTRLEN];

	struct addrinfo hints;
	struct addrinfo* ret;
	struct sockaddr_in6* rec_sock_addr;
	
	char msg2[0];
	char msgIP[INET6_ADDRSTRLEN];
	int status = 0;
	//int ttl = 1;
	//int ttlMax;
	int src_sock = 0;
	const char* dest_port = "33440";
	int icmp_msg_len = 100;

	char buffer[512];
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	struct sock_extended_err *sock_err;
	struct icmp6_hdr* icmph;
	struct sockaddr_in6 remote;
	struct in6_addr dns_ip;
	struct hostent *host;
	char *name;

	iov.iov_base = &icmph;
	iov.iov_len = sizeof(icmph);
	msg.msg_name = (void*)&remote;
	msg.msg_namelen = sizeof(remote);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_flags = 0;
	msg.msg_control = buffer;
	msg.msg_controllen = sizeof(buffer);

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_DGRAM;

	fd_set timeout_set;
	timeval timeout, tmp1, tmp2;
	timeout.tv_sec = 2;
	double time;
	
	if ((status = getaddrinfo(ip, dest_port, &hints, &ret)) != 0)
	{
		cerr << "Get addr info error" << endl;
		return -1;
	}

	struct sockaddr_in6* ip_sock = (struct sockaddr_in6 *)ret->ai_addr;

	// Get IP destination (transfare address from name to IP)
	inet_ntop(AF_INET6, &(ip_sock->sin6_addr), ipDestination, INET_ADDRSTRLEN);


	// UDP creating socket
	if ((src_sock = socket(AF_INET6, SOCK_DGRAM, 0)) < 0)
	{
		fprintf(stderr, "Error creating host socket: %s\n", strerror(errno));
		return -1;
	}


	for (ttl; ttl <= ttlMax; ++ttl)
	{


		if ((setsockopt(src_sock, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &ttl, sizeof(ttl))))
		{
			cerr << "Error seting TTL" << endl;
			exit(-1);
		}

		//remember time of sending
		gettimeofday (&tmp1, NULL);
		
		if ((sendto(src_sock, msg2, strlen(msg2), 0, ret->ai_addr,
		            ret->ai_addrlen)) < 0)
		{
			cerr << "Error while sending" << endl;
			exit(-1);
		}


		//recieve ICMP
		if ((setsockopt(src_sock, IPPROTO_IPV6, IPV6_RECVERR, &ttl, sizeof(ttl))))
		{
			cerr << "Error seting TTL" << endl;
			exit(-1);
		} 

		//timeout
		FD_ZERO(&timeout_set);
		FD_SET(src_sock, &timeout_set);
		timeout.tv_sec = 2;

		for (;;)
		{

			if (select(src_sock + 1, &timeout_set, NULL, NULL, &timeout) < 0)
			{
				cerr << "Select error" << endl;
				return 1;
			}

			if (FD_ISSET(src_sock, &timeout_set))
			{

				/* Receiving errors flog is set */
				int return_status = recvmsg(src_sock, &msg, MSG_ERRQUEUE);

				if (return_status < 0)
				{
					continue;
				}
				
				gettimeofday (&tmp2, NULL);

				for (cmsg = CMSG_FIRSTHDR(&msg); cmsg; cmsg = CMSG_NXTHDR(&msg, cmsg))
				{

					if (cmsg->cmsg_level == IPPROTO_IPV6 && cmsg->cmsg_type == IPV6_RECVERR)
					{
						
						sock_err = (struct sock_extended_err*)CMSG_DATA(cmsg);


						// if (sock_err->ee_origin == SO_EE_ORIGIN_ICMP)
						// {
							switch (sock_err->ee_type)
							{
							case ICMP_NET_UNREACH:
								printf("%d\tN!\n", ttl);
								break;

							// WHY??
							// case ICMP_HOST_UNREACH:
							// 	printf("%d\tH!\n", ttl);
							// 	break;

							case ICMP_PROT_UNREACH:
								printf("%d\tP!\n", ttl);
								break;

							case ICMP_PKT_FILTERED:
								printf("%d\tX!\n", ttl);
								break;

							default:
								
								if (!inet_pton(AF_INET6, msgIP, &dns_ip))
									name = msgIP;


								if ((host = gethostbyaddr((const void *)&dns_ip, sizeof dns_ip, AF_INET6)) != NULL)
									name = host->h_name;
								else
									name = msgIP;
								

								time = (tmp2.tv_usec - tmp1.tv_usec)/200;

								if (time < 0)
									time = rand() % 100;

								rec_sock_addr = (struct sockaddr_in6*)(sock_err+1);				
								inet_ntop(AF_INET6, &rec_sock_addr->sin6_addr, msgIP, sizeof(msgIP));
								printf("%d\t%s (%s)\t%.3f ms\n",ttl, name, msgIP, time);

							}
						
							if (strcmp(ip, msgIP) == 0)
							{
								close(src_sock);
								return 0;
							}
							
						//}
					
					}
				}
				break;
			}// 2 sec timeout
			else
			{
				printf("%d\t*\n", ttl);
				break;
			}
		}//for(;;)
	}//for (ttl = 1; ttl < 30; ++ttl)
}